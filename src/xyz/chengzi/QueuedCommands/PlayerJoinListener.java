package xyz.chengzi.QueuedCommands;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener
{
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        List<String> commands = QueuedCommands.commandsMap.remove(event.getPlayer()
                .getUniqueId());
        
        if (commands == null)
            return;
        for (String command : commands)
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
    }
}
